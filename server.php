<?php
session_start();

if (isset($_POST['email']) && isset($_POST['password'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];


    if ($email === 'johnsmith@gmail.com' && $password === '1234') {

        $_SESSION['loggedin'] = true;
        $_SESSION['email'] = $email;

        header('Location: server.php');
        exit;
    } else {

        $_SESSION['login_error'] = 'Incorrect email or password.';
        header('Location: index.php');
        exit;
    }
} elseif (isset($_GET['logout'])) {

    session_destroy();
    header('Location: index.php');
    exit;
}

if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] === true) {
    $email = $_SESSION['email'];
    ?>
    <!DOCTYPE html>
    <html>
    <head>
        <title>Server Page</title>
    </head>
    <body>
        <h2>Hello, <?php echo $email; ?>!</h2>
        <a href="server.php?logout=true">Logout</a>
    </body>
    </html>
    <?php
} else {
    header('Location: index.php');
    exit;
}
?>
